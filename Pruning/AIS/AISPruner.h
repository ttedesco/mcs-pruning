/*
 * AISPruner.h
 *
 *  Created on: Feb 24, 2011
 *      Author: rgreen
 */

#ifndef AISPRUNER_H_
#define AISPRUNER_H_

#include "Pruner.h"
#include "Antibody.h"
class AISPruner : public Pruner{
    public:
        AISPruner();

        AISPruner(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l,
                    double _pLoad, bool ul=false, double _cloneFactor = 0.1, double _pMut = -2.5);
        virtual ~AISPruner();

        void Init(int popSize, int generations, Classifier* lp, std::vector<Generator> g, std::vector<Line> l,
                    double _pLoad, bool ul=false, double _cloneFactor = 0.1, double _pMut = -2.5);

        void Reset(int np, int nt);
        void Prune(MTRand& mt);

    protected:
        bool isConverged();

        void initPopulation(MTRand& mt);
        void evaluateFitness();
        void selectAntibodies();	// Select N Best Antibodies
        void cloneAntibodies(MTRand& mt);  // Copy, Mutate, and Add to population
        void addClonesToPopulation();
        void sortAndTruncate();
        void calculateAffinity();

        void replaceAntibodies();
        void clearVectors();
        void clearTimes();

        static bool sortPop(const Antibody& elem1, const Antibody& elem2);

        int iterations;

        double gBestValue, pMut;
        double cloneFactor;

        double initTime, fitnessTime, cloneTime, sortTime, affinityTime;

        std::vector<Antibody> antibodies;
        std::vector<Antibody> clones;
};

#endif /* AISPRUNER_H_ */
